## 一对一

### sql
```sql linenums="1"
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  username varchar(100) comment '用户名',
  dept_id bigint(20) comment '所属部门 id',
  `status` char(1) DEFAULT NULL COMMENT '状态 (0:正常 1:停用)',
  `create_by` varchar(150) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(150) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `deleted` int(10) DEFAULT NULL COMMENT '逻辑删除',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

INSERT INTO sys_user (id, username, dept_id, status, create_by, create_time, update_by, update_time, remark, deleted, version) VALUES(1, 'superAdmin', 1, '0', '1', '2024-02-09 05:50:41', '1', '2024-02-09 05:50:41', NULL, 0, 1);

CREATE TABLE `sys_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(100) comment '部门名称',
  `status` char(1) DEFAULT NULL COMMENT '状态 (0:正常 1:停用)',
  `create_by` varchar(150) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(150) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `deleted` int(10) DEFAULT NULL COMMENT '逻辑删除',
  `version` int(10) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='系统部门表';
INSERT INTO sys_dept (id, name, status, create_by, create_time, update_by, update_time, remark, deleted, version) VALUES(1, '研发部', '0', '1', '2024-02-09 05:52:01', '1', '2024-02-09 05:52:01', NULL, 0, 1);
```

### dto
```java linenums="1"
package com.laolang.zentao4j.modules.system.dto;

import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class SysUserDto {

    private Long id;
    private String username;
    private DeptBean dept;

    @Data
    public static class DeptBean{
        private Long id;
        private String name;
    }

}
```

### mapper 接口
```java
SysUserDto selectById(@Param("id")Long id);
```
### mapper xml
```xml linenums="1"
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.laolang.zentao4j.modules.system.mapper.SysUserMapper">

  <resultMap id="SysUserDtoResultMap" type="com.laolang.zentao4j.modules.system.dto.SysUserDto">
    <id column="id" property="id" />
    <result column="username" property="username" />

    <association property="dept" javaType="com.laolang.zentao4j.modules.system.dto.SysUserDto$DeptBean">
      <id column="dept_id" property="id" />
      <result column="dept_name" property="name" />
    </association>
  </resultMap>

  <select id="selectById" resultMap="SysUserDtoResultMap">
    select
      t1.id ,
      t1.username ,
      t1.dept_id ,
      t2.name dept_name
    from
      sys_user t1
    left join sys_dept t2 on
      t1.dept_id = t2.id
    where
    t1.id = #{id}
  </select>
</mapper>
```

## 一对多
### dto
```java linenums="1"
package com.laolang.zentao4j.modules.system.dto;

import java.util.List;
import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class SysDictInfoDto {

    private Long id;
    private String type;
    private String groupCode;
    private List<SysDictDataBean> datas;

    @Data
    public static class SysDictDataBean{

        private Long id;
        private String type;
        private String groupCode;
        private String value;

    }

}
```

### mapper 接口
```java linenums="1"
List<SysDictInfoDto> selectDictInfoList(@Param("params") SysDictType params);
```

### mapper xml
```xml linenums="1"
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.laolang.zentao4j.modules.system.mapper.SysDictTypeMapper">

  <resultMap id="SysDictInfoDtoResultMap" type="com.laolang.zentao4j.modules.system.dto.SysDictInfoDto">
    <id column="id" property="id" />
    <result column="type" property="type" />
    <result column="group_code" property="groupCode" />
    <collection property="datas" ofType="com.laolang.zentao4j.modules.system.dto.SysDictInfoDto$SysDictDataBean">
      <id column="data_id" property="id" />
      <result column="data_type" property="type" />
      <result column="data_group_code" property="groupCode" />
      <result column="data_value" property="value" />
    </collection>
  </resultMap>

  <select id="selectDictInfoList" resultMap="SysDictInfoDtoResultMap">
    select
      t1.id,
      t1.`type` ,
      t1.group_code ,
      t2.id data_id,
      t2.`type` data_type,
      t2.group_code data_group_code,
      t2.value data_value
    from
      sys_dict_type t1
    left join sys_dict_data t2 on
      t1.`type` = t2.`type`
      and t1.group_code = t2.group_code
    where
      t1.`type` = #{params.type}
      and t1.group_code = #{params.groupCode}
  </select>
</mapper>
```