## @Param
=== "mapper 接口"

    ```java
    SysDictType selectById(@Param("id") Long id);
    ```

=== "mapper xml"

    ``` xml
    <select id="selectById" resultType="com.laolang.zentao4j.modules.system.entity.SysDictType">
        select * from sys_dict_type where id = #{id}
    </select>
    ```

## Map
=== "mapper 接口"

    ```java
    List<SysDictType> selectListByMap(Map<String,Object> params);
    ```

=== "mapper xml"

    ``` xml
    <select id="selectListByMap" resultType="com.laolang.zentao4j.modules.system.entity.SysDictType">
        select * from sys_dict_type where type = #{type} and group_code = #{groupCode}
    </select>
    ```

## 对象传参
=== "mapper 接口"

    ```java
    List<SysDictType> selectListByEntity(@Param("entity") SysDictType entity);
    ```

=== "mapper xml"

    ``` xml
    <select id="selectListByEntity" resultType="com.laolang.zentao4j.modules.system.entity.SysDictType">
        select * from sys_dict_type where type = #{entity.type} and group_code = #{entity.groupCode}
    </select>
    ```


## list 传参

!!! note "注意"
    参考:[Mybatis传递List集合](https://blog.csdn.net/jushisi/article/details/106674079)
    
    此处只记录两种

### List 传参
=== "mapper 接口"

    ```java
    List<SysDictType> selectListByList01(@Param("types") List<String> types);
    ```

=== "mapper xml"

    ``` xml
    <select id="selectListByList01" resultType="com.laolang.zentao4j.modules.system.entity.SysDictType">
        select * from sys_dict_type where type in
        <foreach collection="types" item="item" separator="," open="(" close=")">
        #{item}
        </foreach>
    </select>
    ```
### JavaBean 包装 List
=== "mapper 接口"

    ```java
    List<SysDictType> selectListByList02(@Param("params") SysDictTypeDto dto);
    ```

=== "mapper xml"

    ``` xml
    <select id="selectListByList02" resultType="com.laolang.zentao4j.modules.system.entity.SysDictType">
        select * from sys_dict_type where type in
        <foreach collection="params.types" item="item" separator="," open="(" close=")">
        #{item}
        </foreach>
    </select>
    ```

=== "dto"

    ```java
    package com.laolang.zentao4j.modules.system.dto;

    import java.util.List;
    import lombok.Data;

    /**
    * xxx.
    *
    * @author laolang
    * @version 0.1
    */
    @Data
    public class SysDictTypeDto {

        private List<String> types;

    }
    ```